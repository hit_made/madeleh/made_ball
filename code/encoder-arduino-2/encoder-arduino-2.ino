/*******Interrupt-based Rotary Encoder Sketch*******
by Simon Merrett, based on insight from Oleg Mazurov, Nick Gammon, rt, Steve Spence
*/

/*
 * Some Theory.
 * assuming wheel size is 20mm, and there are 24 steps, each inc is equivlent to 2.6mm
 * thus, the percision needed here needs to be in cms, we can divide the result by 100.
 */

/*essentially, we need to have the arudino 
 * cosntantly sample the current position and speed, and transmit it via i2c
 * issue is that i2c is a blocking event, so we need to ensure that we don't fuck it up.
 * 
 */

#include <Wire.h>
#include <stdint.h>


#define outputL 8
#define outputR 9

#define ONTIME 100

#define ENABLE_DEBUG

#define SLAVE_I2C_ADDR 22

#define POSITION_LIMIT  500UL

#define DEFAULT_CAL_VALUE 11460

#define I2C_PREAMBLE 0xFEEDABBA


#define inrange(d,x,y) ((d>=x)&&(d<=y))?1:0

/*--------------i2c related--------*/
typedef struct __attribute__((packed)){
  uint32_t nPreamble;
  uint32_t nCalValue;
  union{
    struct __attribute__((packed)){
      unsigned bResetDev : 1;
      unsigned bNewCalValue :1;
      unsigned nRsvd : 6;
    };
    uint8_t nRequests;
  };
  uint8_t nSeqNum; 
  uint16_t nChecksum;
} i2c_rx_msg_type;


typedef struct __attribute__((packed)) {
  uint32_t nPreamble; 
  int32_t nCurrentpos;
  uint8_t bNewPos;
  uint8_t nSeqNum;
  uint16_t nChecksum;
} i2c_tx_msg_type;




volatile i2c_rx_msg_type i2c_rx_msg;
volatile i2c_tx_msg_type i2c_tx_msg;

volatile uint8_t nRxMsgBuf[100];

volatile bool bI2cNewMsg=false;

/*---------------------------------*/



static int pinA = 2; // Our first hardware interrupt pin is digital pin 2
static int pinB = 3; // Our second hardware interrupt pin is digital pin 3
volatile byte aFlag = 0; // let's us know when we're expecting a rising edge on pinA to signal that the encoder has arrived at a detent
volatile byte bFlag = 0; // let's us know when we're expecting a rising edge on pinB to signal that the encoder has arrived at a detent (opposite direction to when aFlag is set)
volatile byte reading = 0; //somewhere to store the direct values we read from our interrupt pins before checking to see if we have moved a whole detent

volatile bool bGoLeft =0;
volatile bool bGoRight =0;

unsigned long nCurrTime;
unsigned long nLastEventTime = 0;

unsigned long nPrevTime;



long nSpeed;
long nOldPos=0;
long encoderPos=0;

volatile long nEstPos=0;
volatile long nPrevPos=0;
volatile uint32_t nCurrCalValue = DEFAULT_CAL_VALUE;

bool bFirsttime = true;



void init_i2c_message()
{

    //setup premble
  i2c_tx_msg.nPreamble = I2C_PREAMBLE;
  i2c_tx_msg.nSeqNum =0;
  i2c_tx_msg.bNewPos = 0;
}



void setup() {
  pinMode(pinA, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(pinB, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  attachInterrupt(0,PinA,RISING); // set an interrupt on PinA, looking for a rising edge signal and executing the "PinA" Interrupt Service Routine (below)
  attachInterrupt(1,PinB,RISING); // set an interrupt on PinB, looking for a rising edge signal and executing the "PinB" Interrupt Service Routine (below)

  //setup the output pins.
  pinMode (LED_BUILTIN,OUTPUT);


  //setup i2c communication.
  Wire.begin(SLAVE_I2C_ADDR); //todo: read one of the pins to have this code generic.

  
  init_i2c_message();
  //connect function to handle incoming i2c bytes.
  Wire.onReceive(i2c_rx);
  Wire.onRequest(i2c_tx);
  

#ifdef ENABLE_DEBUG
  Serial.begin(115200); // start the serial monitor link
  Serial.println("I am alive!!!");
#endif
}



/*-------------ENCODER PIN INTERRUPTS---------------------------*/
void PinA(){
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0xC; // read all eight pin values then strip away all but pinA and pinB's values
  if(reading == B00001100 && aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos --; //decrement the encoder's position count
    bGoRight=1;
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00000100) bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void PinB(){
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0xC; //read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00001100 && bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos ++; //increment the encoder's position count
    bGoLeft =1;
    
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00001000) aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  sei(); //restart interrupts
}
/*------------------------------------------------------------*/




/*--------------------I2C Related Functions-------------------*/
void i2c_rx(int howMany) {
    //step one: collect bytes from i2c.
  int i=0;

  while(Wire.available())
  {
    nRxMsgBuf[i++] = Wire.read();
  }

  if(i == sizeof(i2c_rx_msg_type))
  {
  //copy message to local struct.
  memcpy((uint8_t*)&(i2c_rx_msg.nPreamble),&nRxMsgBuf[0],sizeof(i2c_rx_msg_type));

  bI2cNewMsg=true;
  }
#ifdef ENABLE_DEBUG
  else
  {
    Serial.print("error! recieved i2c: ");
    Serial.println(i);
  }
#endif
}


void i2c_tx(){
  Wire.write((uint8_t*)&i2c_tx_msg,sizeof(i2c_tx_msg_type));

  //clear new pos flag.
  i2c_tx_msg.bNewPos = 0;

  //increment seqnumber.
  i2c_tx_msg.nSeqNum++;
}


uint8_t HandleI2cMsg()
{

  //step 1: verify msg is aligned.
  if(i2c_rx_msg.nPreamble != I2C_PREAMBLE)
  {
    return 1; 
  }

  //step 2: test what needs to be handled.
  if(i2c_rx_msg.bResetDev)
  {
    //reset of current values was requested.
    nEstPos = 0;

#ifdef ENABLE_DEBUG
    Serial.println("Position Reset.");
#endif

  }
  if(i2c_rx_msg.bNewCalValue)
  {
    nCurrCalValue = i2c_rx_msg.nCalValue;
  }

  return 0;
}

uint16_t generate_checksum(uint8_t* pData,uint16_t nSize)
{
  uint16_t checksum=0;
  for(int i=0;i<nSize;i++)
  {
    checksum += *(pData+i);
  }

  return checksum;
}


void build_i2c_tx_msg()
{

  //copy current pos to msg.
  i2c_tx_msg.nCurrentpos = nEstPos;


  //build tx
  i2c_tx_msg.nChecksum = generate_checksum((uint8_t*)&(i2c_tx_msg.nPreamble),sizeof(i2c_tx_msg_type)-2);

}




void solve_encoder_position()
{
   //calculate speed      
  nSpeed = (float)(encoderPos-nOldPos)*nCurrCalValue; //time delta division removed for calc optimization.
  nOldPos = encoderPos;

  if(bFirsttime)
  {
    bFirsttime = false;
    nSpeed = 0; 
  }

  if(nSpeed != 0)
  {
    //save the previous position
    nPrevPos = nEstPos;
    nEstPos+= (nSpeed)/10000; //note that we don't multiply by time siince we see speed as a ratio.

    //check if new position is out of logical range.
    if(!inrange(nEstPos,-POSITION_LIMIT,POSITION_LIMIT))
    {
      nEstPos = nPrevPos;
    }

#ifdef ENABLE_DEBUG
    Serial.println(nSpeed);
    Serial.print("Pos =");
    Serial.println(nEstPos);
#endif
  
    i2c_tx_msg.bNewPos=1;
  }
 
}



void loop(){
  
  //get current time.
  nCurrTime = millis();
  
  //check if we received a new i2c message to handle.
  if(bI2cNewMsg)
  {
    bI2cNewMsg = false;
    HandleI2cMsg();
  }  

  //analyze a new position.
  solve_encoder_position();
  
  //generate an i2c message.
  build_i2c_tx_msg();
  //this should always be last.
  nPrevTime = nCurrTime;
}
