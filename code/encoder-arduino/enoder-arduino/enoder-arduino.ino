
 #define outputA 6
 #define outputB 7

#define outputL 8
#define outputR 9

#define ONTIME 100
 
 int counter = 0; 
 int aState;
 int aLastState;  
 unsigned long nCurrentEventTime;
 void setup() { 
   pinMode (outputA,INPUT_PULLUP);
   pinMode (outputB,INPUT_PULLUP);
   pinMode (outputL,OUTPUT);
   pinMode (outputR,OUTPUT);
   
   Serial.begin (9600);
   // Reads the initial state of the outputA
   aLastState = digitalRead(outputA);   
 } 
 void loop() { 

   if((millis()-nCurrentEventTime) > ONTIME)
   {
       digitalWrite(outputR,LOW);
       digitalWrite(outputL,LOW);
   }
  
   aState = digitalRead(outputA); // Reads the "current" state of the outputA
   // If the previous and the current state of the outputA are different, that means a Pulse has occured
   if (aState != aLastState){     
     // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
     if (digitalRead(outputB) != aState) { 
       counter ++;
       digitalWrite(outputR,HIGH);
       digitalWrite(outputL,LOW);
       nCurrentEventTime = millis();
       
     } else {
       digitalWrite(outputR,LOW);
       digitalWrite(outputL,HIGH);
       nCurrentEventTime = millis();
       counter --;
     }
     Serial.print("Position: ");
     Serial.println(counter);
   } 
   aLastState = aState; // Updates the previous state of the outputA with the current state

   
 }
